/*
 * =====================================================================================
 *
 *       Filename:  util.c
 *
 *    Description: utilities 
 *
 *        Version:  1.0
 *        Created:  03/23/2020 11:01:17 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Elliott Villars (), elliottvillars@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#include "util.h"
double Log(double target,double base)
{  
    // log(n)/log(2) is log2.  
    return log( target ) / log( base );  
}  
