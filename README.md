
# CIC Compiler  
Dont use this. It is an old and poorly designed prototype. A much better verision, written in Haskell can be found here:
https://gitlab.com/Necryotiks/cicgen
## Automatically generate a Cascaded-Intergrator-Comb filter Verilog file in either decimator or interpolator configuration.  
This project requires Cmake Ver. 3.13 or greater.  
Ubuntu/Debian install:  

    sudo apt-get install cmake

Arch Linux install:

	sudo pacman -S cmake

Gentoo Linux install:

	sudo emerge cmake

Fedora/RHEL/CentOS:
	
	sudo dnf install cmake

Installation instructions:

    cmake -B build 
    cmake --build build  
    sudo cmake --install build  

