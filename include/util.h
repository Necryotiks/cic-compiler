/*
 * =====================================================================================
 *
 *       Filename:  util.h
 *
 *    Description: utilites header 
 *
 *        Version:  1.0
 *        Created:  03/23/2020 07:33:58 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Elliott Villars (), elliottvillars@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
double Log( double target, double base );
